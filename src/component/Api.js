import axios from "axios";

const token =
  "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMDcsImV4cCI6MTYyMDUxODI0Nn0.p-9nBayR-LQ2X526MOki8pK99xyWB17SoEfHqhu8KMQ";
const url = "https://todos-project-api.herokuapp.com";
const authApi = axios.create({
  baseURL: url,
  headers: {
    Authorization: `Bearer ${token}`,
  },
});

export default authApi;
