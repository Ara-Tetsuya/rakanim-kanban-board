import styled from "styled-components";
import backgroundThemes from "../../theme/backgroundTheme";
import borderThemes from "../../theme/borderTheme";
import colorsThemes from "../../theme/colorsTheme";

const Button = styled.button`
  box-sizing: border-box;
  width: auto;
  height: 32px;
  padding: 0.313rem 1rem;
  border: none;
  transition: 0.3s;
  cursor: pointer;
  :focus {
    outline: 0;
  }
`;

export const ButtonCancel = styled(Button)`
  background: #fff;
  color: ${colorsThemes["colorDarkOne"]};
  border: 1px solid ${borderThemes["borderGrayOne"]};
  margin-right: 8px;
`;
export const ButtonSuccess = styled(Button)`
  background: ${backgroundThemes["backgroundGreen2"]};
  color: #fff;
`;
export const ButtonDelete = styled(Button)`
  background: ${backgroundThemes["backgrounRed2"]};
  color: #fff;
`;
