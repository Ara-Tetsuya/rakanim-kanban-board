import React from "react";
import styled from "styled-components";
import colorsThemes from "../../theme/colorsTheme";
import themes from "../../theme/commonThemes";
import StyledCard from "./StyledCard";
import Title from "./TitleCard";

const Months = styled.h4`
  font-size: ${themes["tinyText"]};
  line-height: ${themes["lineHeight20"]};
  color: ${colorsThemes["colorDarkTwo"]};
  margin-bottom: 0.875rem;
`;
const Card = ({ children, title, month }) => {
  return (
    <StyledCard title={title}>
      <Title title={title}>{title}</Title>
      <Months>{month}</Months>
      {children}
    </StyledCard>
  );
};

export default Card;
