import styled, { css } from "styled-components";
import backgroundThemes from "../../theme/backgroundTheme";
import borderThemes from "../../theme/borderTheme";
import colorsThemes from "../../theme/colorsTheme";
import themes from "../../theme/commonThemes";

const CardVariants = (title) => {
  switch (title) {
    case "Group Task 1":
      return css`
        background: ${backgroundThemes["backgroundPink"]};
        border: 1px solid ${borderThemes["borderPink"]};
      `;
    case "Group Task 2":
      return css`
        background: ${backgroundThemes["backgroundUngu"]};
        border: 1px solid ${borderThemes["borderUngu"]};
      `;
    case "Group Task 3":
      return css`
        background: ${backgroundThemes["backgroundBiru"]};
        border: 1px solid ${borderThemes["borderBiru"]};
      `;
    case "Group Task 4":
      return css`
        background: ${backgroundThemes["backgroundHijau"]};
        border: 1px solid ${borderThemes["borderHijau"]};
      `;
    default:
      return css`
        background: ${backgroundThemes["backgroundBlueGray"]};
        border: 1px solid ${borderThemes["borderGrayTwo"]};
      `;
  }
};

const StyledCard = styled.div`
  ${(props) => CardVariants(props.title)};
  color: ${colorsThemes["colorDarkTwo"]};
  padding: 0.875rem;
  border-radius: ${themes["radius4"]};
`;

export default StyledCard;
