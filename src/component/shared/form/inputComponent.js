import styled from "styled-components";
import borderThemes from "../../theme/borderTheme";
import colorsThemes from "../../theme/colorsTheme";
import themes from "../../theme/commonThemes";

export const Label = styled.label`
  font: 400 ${themes["tinyText"]} Open Sans;
  color: ${colorsThemes["colorLightTwo"]};
  width: 100%;
  display: inline-block;
  line-height: 16.34px;
  margin-bottom: 4px;
`;
export const Form = styled.form`
  width: 100%;
`;
const Input = styled.input`
  color: ${colorsThemes["colorDarkTwo"]};
  height: 2.5rem;
  margin-bottom: 8px;
  padding: 10px;
  border: 1px solid ${borderThemes["borderGrayThree"]};
  ::placeholder {
    color: ${colorsThemes["colorLightOne"]};
  }
  :focus {
    border-color: ${colorsThemes["colorDarkOne"]};
    outline: none;
  }
`;

export const InputTitle = styled(Input)`
  font: 600 ${themes["smallText"]} Open Sans;
  width: 100%;
`;

export const InputProgress = styled(Input)`
  font: 600 ${themes["smallText"]} Open Sans;
  width: 6.288rem;
`;
