import React from "react";
import styled from "styled-components";
import borderThemes from "../../theme/borderTheme";
import themes from "../../theme/commonThemes";

const EmptyCard = styled.div`
  background: #fff;
  width: 100%;
  height: 3.5rem;
  border: 1px solid ${borderThemes["borderGrayTwo"]};
  padding: 1rem 1rem 0.75rem;
`;
const EmptyText = styled.p`
  font-size: ${themes["smallText"]};
  font-weight: 400;
  color: #b7bdc9;
`;
const EmptyItems = () => {
  return (
    <EmptyCard>
      <EmptyText>No Task Available</EmptyText>
    </EmptyCard>
  );
};

export default EmptyItems;
