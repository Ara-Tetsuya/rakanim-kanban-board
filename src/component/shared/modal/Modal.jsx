import React, { useEffect, useState } from "react";
import { createPortal } from "react-dom";
import { ModalBody, ModalContainer } from "./modalComponents";
import "./styles.css";

const Portal = ({ children }) => {
  const modalRoot = document.getElementById("modal-root");
  const [element] = useState(document.createElement("div"));
  useEffect(() => {
    modalRoot.appendChild(element);

    return function cleanup() {
      modalRoot.removeChild(element);
    };
  }, [modalRoot, element]);

  return createPortal(children, element);
};

const Modal = ({
  width,
  height,
  padding,
  fade = false,
  children,
  toggle,
  open,
}) => {
  return (
    <Portal>
      {open && (
        <div className={`modal ${fade ? "modal-show" : ""}`} onClick={toggle}>
          <ModalContainer>
            <ModalBody
              width={width}
              height={height}
              padding={padding}
              onClick={(event) => event.stopPropagation()}
            >
              {children}
            </ModalBody>
          </ModalContainer>
        </div>
      )}
    </Portal>
  );
};

export default Modal;
