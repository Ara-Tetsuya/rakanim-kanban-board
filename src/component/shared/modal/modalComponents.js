import styled from "styled-components";
import colorsThemes from "../../theme/colorsTheme";
import themes from "../../theme/commonThemes";

export const ModalContainer = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
`;
export const ModalBody = styled.div`
  position: relative;
  margin: 0 auto;
  background: #fff;
  width: ${({ width }) => width};
  height: ${({ height }) => height};
  border: none;
  overflow-x: hidden;
  overflow-y: auto;
  max-height: 100%;
  padding: ${({ padding }) => padding};
  box-shadow: ${themes["shadow"]};
  z-index: 11;
`;

export const ModalTitle = styled.h1`
  color: ${colorsThemes["colorDarkOne"]};
  font-size: ${themes["mediumText"]};
  font-weight: 700;
  line-height: ${themes["lineHeight24"]};
  margin-bottom: ${({ mb }) => mb};
`;

export const ModalText = styled.p`
  color: ${colorsThemes["colorDarkOne"]};
  font-size: ${themes["smallText"]};
  font-weight: 400;
  line-height: ${themes["lineHeight22"]};
`;

export const ButtonGroup = styled.div`
  position: absolute;
  right: 0;
  bottom: 0;
`;
