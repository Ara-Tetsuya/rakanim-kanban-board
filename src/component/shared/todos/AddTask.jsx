import React from "react";
import ButtonAdd from "../button/ButtonAdd";
import Modal from "../modal/Modal";
import useToggle from "../useToggle";
import ModalAdd from "./ModalAdd";

const AddTask = ({ id }) => {
  const [open, setOpen] = useToggle(false);
  return (
    <>
      <ButtonAdd open={() => setOpen()} />
      {open && (
        <Modal
          width="567px"
          height="302px"
          padding="28px 33px"
          fade
          open={open}
          toggle={setOpen}
        >
          <ModalAdd id={id} cancel={() => setOpen(false)} />
        </Modal>
      )}
    </>
  );
};

export default AddTask;
