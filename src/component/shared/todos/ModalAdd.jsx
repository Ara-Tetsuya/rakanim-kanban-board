import React, { useState } from "react";
import authApi from "../../Api";
import { ButtonCancel, ButtonSuccess } from "../button/ButtonVariants";
import { Form, InputProgress, InputTitle, Label } from "../form/inputComponent";
import { ButtonGroup, ModalTitle } from "../modal/modalComponents";

const ModalAdd = ({ id, cancel }) => {
  const [task, setTask] = useState("");
  const onChangeTask = (e) => setTask(e.target.value);
  const [progress, setProgress] = useState("");
  const onChangeProgress = (e) => setProgress(e.target.value);
  const onSubmitTask = async () => {
    const task = {
      name: task,
      progress_percentage: +progress.slice(0, progress.length - 1),
    };
    const response = await authApi.post(`/todos/${id}/items`);
    const data = await response.data;
    console.log(data);
  };
  return (
    <div className="relative h-100">
      <ModalTitle mb="21px">Create Task</ModalTitle>
      <Form onSubmit={onSubmitTask}>
        <div>
          <Label>Task Name</Label>
          <InputTitle
            type="text"
            placeholder="example: Build rocket to Mars."
            onChange={(e) => onChangeTask(e)}
            value={task}
          />
          <Label>Progress</Label>
          <InputProgress
            type="text"
            placeholder="0%"
            onChange={(e) => onChangeProgress(e)}
            value={progress}
          />
        </div>
        <ButtonGroup>
          <ButtonCancel type="button" onClick={cancel}>
            Cancel
          </ButtonCancel>
          <ButtonSuccess type="submit" onClick={cancel}>
            Save Task
          </ButtonSuccess>
        </ButtonGroup>
      </Form>
    </div>
  );
};

export default ModalAdd;
