import React, { useEffect, useState } from "react";
import styled from "styled-components";
import authApi from "../../Api";
import Card from "../card/Card";
import EmptyItems from "../items/EmptyItems";
import { ItemList } from "../items/ItemCard";
import Items from "../items/Items";
import AddTask from "./AddTask";

const ListTodo = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-template-rows: auto auto;
  grid-column-gap: 1rem;
`;

const Todos = () => {
  const [todos, setTodos] = useState([]);
  const [items, setItems] = useState([]);
  const [id, setId] = useState(1);
  const getTodos = async () => {
    const response = await authApi.get("/todos");
    const data = await response.data;
    setTodos(data);
  };

  const getItems = async (id) => {
    const response = await authApi.get(`/todos/${id}/items`);
    const data = await response.data;
    setItems(data);
  };

  useEffect(() => {
    getTodos();
  }, []);

  useEffect(() => {
    getItems(id);
  }, [id]);

  return (
    <ListTodo>
      {todos &&
        todos.map((todo) => {
          return (
            <Card
              key={todo["id"]}
              title={todo["title"]}
              month={todo["description"]}
            >
              {items > 0 ? (
                items
                  .filter((item) => item["todo_id"] === todo["id"])
                  .map((itemValue) => {
                    return (
                      <ItemList key={itemValue["id"]}>
                        <Items
                          id={setId(todo["id"])}
                          title={itemValue["name"]}
                          progress={itemValue["progress_percentage"]}
                        />
                      </ItemList>
                    );
                  })
              ) : (
                <EmptyItems />
              )}
              <AddTask id={todo["id"]} />
            </Card>
          );
        })}
    </ListTodo>
  );
};

export default Todos;
